import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { APISelectionComponent } from './apiselection.component';

describe('APISelectionComponent', () => {
  let component: APISelectionComponent;
  let fixture: ComponentFixture<APISelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ APISelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(APISelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
