import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class AppService {
 
  url1             = "http://127.0.0.1:5002/cognitive-insights/sentiment-analysis"; //sentiment-analysis
  url2             = " http://127.0.0.1:5001/cognitive-insights/entity-recognition"; //named-entities
  url3             = " http://127.0.0.1:5004/cognitive-insights/regex";//RegexURL
  url4             = "http://127.0.0.1:4001/cognitive-insights/semantic-search";//Semantic
  url7             = "https://words.bighugelabs.com/api/2/322df4a639b4b865b4eb3669e289b7eb/";
  ActualAPIStatus  = new BehaviorSubject<any>("");
  sentences        = new BehaviorSubject<any>("");
  Res1             = new BehaviorSubject<any>(""); 
  Res2             = new BehaviorSubject<any>(""); 
  Res3             = new BehaviorSubject<any>("");
  Res4             = new BehaviorSubject<any>("");
  Res5             = new BehaviorSubject<any>("");
  Res6             = new BehaviorSubject<any>("");
  APIStatus        = {
                      "SentimentId":{"Description":"Sentiment Analysis","value":false},
                      "EntitiesId" :{"Description":"Named entities","value":false},
                      "ReGexId"    :{"Description":"Regular Expression (ReGex extraction)","value":false},
                      "SemanticId" :{"Description":"Semantic Search","value":false}
                     };
  httpOptions      = {
                      headers: new HttpHeaders({'Content-Type':'application/json'})
                     }

  constructor(private http: HttpClient) {}

  get isActualAPIStatus() {
    return this.ActualAPIStatus.asObservable();
  }
  get isSentences() {
    return this.sentences.asObservable();
  }
  get isRes1() {
    return this.Res1.asObservable();
  }
  get isRes2() {
    return this.Res2.asObservable();
  }
  get isRes3() {
    return this.Res3.asObservable();
  }
  get isRes4() {
    return this.Res4.asObservable();
  }
  get isRes5() {
    return this.Res5.asObservable();
  }
  get isRes6() {
    return this.Res6.asObservable();
  }

  getSynonyms(data) : Observable<any> {
    return this.http.get<any>(this.url7 + data + "/json")
  }

  callFirst(data) : Observable<any> {
    let body = {text:data};
    return this.http.post<any>(this.url1,body, this.httpOptions)
  }
  callSecond(data) : Observable<any> {
    let body = {text:data};
    return this.http.post<any>(this.url2,body, this.httpOptions)
  }
  callThird(data) : Observable<any> {
    let body = {text:data};
    return this.http.post<any>(this.url3,body, this.httpOptions)
  }
  callFour(data:FormData)  : Observable<any> {
    console.log("data received", data.get("text"))  
    return this.http.post<any>(this.url4,data)
  }
}
