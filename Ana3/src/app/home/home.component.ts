import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { AppService } from '../app.service';
import * as bootstrap from "bootstrap";
import * as $ from 'jquery'
@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
    file: any;
    Api2 = {};
    SentimentText = "";
    NamedentitiesText = "";
    ReGexText = "";
    SemanticText = "";
    wordsen = [];
    text1;
    text2;
    text3;
    text4;
    text5;
    Text;
    Response1;
    Response2;
    Response3;
    Response4;
    Response5;
    Response6;
    Response7;
    Response8;
    mainNav;
    IsSentiment;
    IsNamedentities;
    ActualAPIStatus;
    IsReGex;
    Searchword;
    IsSemantic;
    str;
    AllSearchword;

    constructor(private router: Router, private spinner: NgxSpinnerService, private appService: AppService) { }

    //For toggling tables using up and down arrow.
    ngOnInit() {
        let ref = this;
        $(document).on("click", "i", function () {
            switch (this.id) {
                case "Sentiment":
                    if (ref.text1) {
                        $("#" + this.id + "-Card").toggleClass("open");
                    }
                    ref.toggleCard(this.id);
                    break;
                case "Namedentities":
                    if (ref.text2) {
                        $("#" + this.id + "-Card").toggleClass("open");
                    }
                    ref.toggleCard(this.id);
                    break;
                case "ReGex":
                    if (ref.text3) {
                        $("#" + this.id + "-Card").toggleClass("open");
                    }
                    ref.toggleCard(this.id);
                    break;
                case "Semantic":
                    if (ref.text4) {
                        $("#" + this.id + "-Card").toggleClass("open");
                    }
                    ref.toggleCard(this.id);
                    break;
                default:
                    if (ref.text5) {
                        $("#" + this.id + "-Card").toggleClass("open");
                    }
                    ref.toggleCard(this.id);
                    break;
            }
        });
    }

    // Toggle function use for toggling table with arrow.
    toggleCard(id) {
        if ($("#" + id).hasClass('clicked')) {
            $("#" + id).removeClass('clicked');
            $("#" + id).addClass('fa-chevron-down');
            $("#" + id).removeClass('fa-chevron-up');
        }
        else {
            $("#" + id).addClass('clicked');
            $("#" + id).removeClass('fa-chevron-down');
            $("#" + id).addClass('fa-chevron-up');
        }
    }

    //Get the uploaded file.
    onFileChange(event) {
        if (event.target.files.length > 0) {
            this.file = event.target.files[0];
        }
    }

    //Clear the results and close the tables.
    Close(APIIndex, WrapperId, TableId) {
        let TId = $("#" + TableId);
        if ($("#" + TableId).hasClass('open')) {
            TId.removeClass('open');
        }
        let WId = $("#" + WrapperId);
        if ($("#" + WrapperId).hasClass('open')) {
            WId.removeClass('open');
        }
        switch (APIIndex) {
            case 1:
                this.SentimentText = "";
                break;
            case 2:
                this.NamedentitiesText = "";
                break;
            case 3:
                this.ReGexText = "";
                break;
            case 4:
                this.SemanticText = "";
                break;
            default:
                this.Text = "";
        }
    }

    makeBold(input, wordsToBold) {
        return input.replace(new RegExp('(\\b)(' + wordsToBold.join('|') + ')(\\b)', 'ig'), '$1<b>$2</b>$3');
    }

    //Open the table and show output from API callback
    onSubmit(APIIndex, WrapperId, TableId) {
        let TId = $("#" + TableId);
        if ($("#" + TableId).hasClass('open')) {
            TId.removeClass('open');
        }
        let WId = $("#" + WrapperId);
        if ($("#" + WrapperId).hasClass('open')) {
            WId.removeClass('open');
        }
        switch (APIIndex) {
            case 1:
                this.text1 = this.SentimentText;
                if (this.text1) {
                    this.spinner.show();
                    this.appService.callFirst(this.text1).subscribe(res => {
                        this.Response1 = res;
                        console.log(res);
                        this.spinner.hide();
                    });
                    WId.addClass('open');
                    TId.addClass('open');
                }
                else {
                    alert("Please enter text to analyze.");
                }
                break;
            case 2:
                this.text2 = this.NamedentitiesText;
                if (this.text2) {
                    this.spinner.show();
                    this.appService.callSecond(this.text2).subscribe(res => {
                        this.Response2 = {}
                        for (let key in res["named_entity_resolution"]) {
                            if (res["named_entity_resolution"][key].length) {
                                this.Response2[key] = res["named_entity_resolution"][key];
                            }
                        }
                        console.log(res);
                        this.spinner.hide();
                        WId.addClass('open');
                        TId.addClass('open');
                    });
                }
                else {
                    window.alert("Please enter text to analyze.");
                }

                break;
            case 3:
                this.text3 = this.ReGexText;
                if (this.text3) {
                    this.spinner.show();
                    this.appService.callThird(this.text3).subscribe(res => {
                        this.Response3 = {}
                        for (let key in res[0]) {
                            if (res[0][key].length) {
                                this.Response3[key] = res[0][key];
                            }
                        }
                        console.log(res);
                        this.spinner.hide();
                        WId.addClass('open');
                        TId.addClass('open');
                    });
                }
                else {
                    window.alert("Please enter text to analyze.");
                }
                break;
            case 4:
                this.text4 = this.SemanticText;
                if (this.text4) {
                    if (this.Searchword) {
                        this.spinner.show();
                        this.Response4 = {}
                        const formdata = new FormData();

                        let queryObj = { "text": this.text4, "searched_key": this.Searchword };
                        formdata.append("text", this.text4);
                        formdata.append("searched_key", this.AllSearchword);
                        //console.log(formdata)
                        this.appService.callFour(formdata).subscribe(res => {
                            //console.log(res);
                            this.Response4 = res["semantic_word_search"][0];
                            let Synonyms = res["semantic_word_search"][0]['found'];
                            let wordToBold = [];
                            let words = this.text4.split(" ");
                            for (let w of words) {
                                for (let s of Synonyms) {
                                    if (w.replace(/[^a-zA-Z ]/g, "").match(RegExp(s))) {
                                        wordToBold.push(w.replace(/[^a-zA-Z ]/g, ""));
                                        break;
                                    }
                                }
                            }
                            wordToBold.push(this.Searchword);
                            document.getElementById('Text1').innerHTML = this.makeBold(this.text4, wordToBold);
                            this.spinner.hide();
                            WId.addClass('open');
                            TId.addClass('open');
                        })
                    }
                    else {
                        window.alert("Please enter search word.");
                    }
                }
                else {
                    window.alert("Please enter text to analyze.");
                }
                break;
            default:
                this.text5 = this.Text
                $('#sentence ul li.active').removeClass('active');
                $('#sentence .collapse.show').removeClass('show');
                if (this.text5) {
                    if (this.AllSearchword) {
                        this.spinner.show();
                        this.Response5 = {};
                        this.Response6 = {};
                        this.Response7 = {};
                        this.Response8 = {};
                        let finalAPIDic = {};
                        let APIDic = this.appService.APIStatus;
                        for (let key in APIDic) {
                            finalAPIDic[key] = APIDic[key];
                            finalAPIDic[key]['value'] = true;
                        }
                        this.ActualAPIStatus = finalAPIDic;
                        this.appService.callFirst(this.text5).subscribe(res => {
                            console.log(res)
                            this.Response5 = res;
                            this.appService.callSecond(this.text5).subscribe(res => {
                                console.log(res)
                                for (let key in res["named_entity_resolution"]) {
                                    if (res["named_entity_resolution"][key].length) {
                                        this.Response6[key] = res["named_entity_resolution"][key];
                                    }
                                }
                                this.appService.callThird(this.text5).subscribe(res => {
                                    console.log(res)
                                    for (let key in res[0]) {
                                        if (res[0][key].length) {
                                            this.Response7[key] = res[0][key];
                                        }
                                    }
                                    const formdata = new FormData();

                                    let queryObj = { "text": this.text5, "searched_key": this.Searchword };
                                    formdata.append("text", this.text5);
                                    formdata.append("searched_key", this.AllSearchword);
                                    console.log(this.AllSearchword)
                                    this.appService.callFour(formdata).subscribe(res => {
                                        console.log(res)
                                        this.Response8 = res["semantic_word_search"][0];
                                        let Synonyms = res["semantic_word_search"][0]['found'];
                                        let wordToBold = [];
                                        let words = this.text5.split(" ");
                                        for (let w of words) {
                                            for (let s of Synonyms) {
                                                if (w.replace(/[^a-zA-Z ]/g, "").match(RegExp(s))) {
                                                    wordToBold.push(w.replace(/[^a-zA-Z ]/g, ""));
                                                    break;
                                                }
                                            }
                                        }
                                        wordToBold.push(this.Searchword);
                                        document.getElementById('Text2').innerHTML = this.makeBold(this.text5, wordToBold);
                                        this.spinner.hide();
                                        WId.addClass('open');
                                        TId.addClass('open');
                                        $("#EntitiesId").addClass("show");
                                            $("#sentence li").first().addClass("active");
                                            $(document).on('click', '#sentence ul li', function () {
                                                var $el = $(this);
                                                $el.siblings().removeClass('active');
                                                $el.addClass('active');
                                        });
                                    });
                                });
                            });
                        });
                    }
                    else {
                        window.alert("Please enter search word.");
                    }
                }
                else {
                    window.alert("Please enter text to analyze.");
                }
        }
    }
}
